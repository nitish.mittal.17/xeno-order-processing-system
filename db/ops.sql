-- MySQL dump 10.13  Distrib 5.7.18, for osx10.11 (x86_64)
--
-- Host: localhost    Database: ops
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order_data_daily`
--

DROP TABLE IF EXISTS `order_data_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_data_daily` (
  `contact_number` varchar(10) NOT NULL,
  `_customer` tinyint(4) DEFAULT NULL,
  `_time_slot` tinyint(4) DEFAULT NULL,
  `_store_location` tinyint(4) DEFAULT NULL,
  `time_slot` int(11) NOT NULL,
  `store_location` int(11) NOT NULL,
  `order_count` int(10) unsigned DEFAULT NULL,
  `amount_spent` float DEFAULT NULL,
  `order_time` date NOT NULL,
  `product_count` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`contact_number`,`time_slot`,`store_location`,`order_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_data_daily`
--

LOCK TABLES `order_data_daily` WRITE;
/*!40000 ALTER TABLE `order_data_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_data_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_data_monthly`
--

DROP TABLE IF EXISTS `order_data_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_data_monthly` (
  `contact_number` varchar(10) NOT NULL,
  `_customer` tinyint(4) DEFAULT NULL,
  `_time_slot` tinyint(4) DEFAULT NULL,
  `_store_location` tinyint(4) DEFAULT NULL,
  `time_slot` int(11) NOT NULL,
  `store_location` int(11) NOT NULL,
  `order_count` int(10) unsigned DEFAULT NULL,
  `amount_spent` float DEFAULT NULL,
  `order_time` date NOT NULL,
  `product_count` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`contact_number`,`time_slot`,`store_location`,`order_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_data_monthly`
--

LOCK TABLES `order_data_monthly` WRITE;
/*!40000 ALTER TABLE `order_data_monthly` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_data_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_data_yearly`
--

DROP TABLE IF EXISTS `order_data_yearly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_data_yearly` (
  `contact_number` varchar(10) NOT NULL,
  `_customer` tinyint(4) DEFAULT NULL,
  `_time_slot` tinyint(4) DEFAULT NULL,
  `_store_location` tinyint(4) DEFAULT NULL,
  `time_slot` int(11) NOT NULL,
  `store_location` int(11) NOT NULL,
  `order_count` int(10) unsigned DEFAULT NULL,
  `amount_spent` float DEFAULT NULL,
  `order_time` date NOT NULL,
  `product_count` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`contact_number`,`time_slot`,`store_location`,`order_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_data_yearly`
--

LOCK TABLES `order_data_yearly` WRITE;
/*!40000 ALTER TABLE `order_data_yearly` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_data_yearly` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-30 16:07:07
