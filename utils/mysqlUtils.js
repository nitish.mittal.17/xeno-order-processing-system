/**
 * sample config
 * {
 *      connectionLimit: 100,
        host     : '127.0.0.1',
        user     : 'xxxx',
        password : 'xxxx',
        database : 'xxxx'
 * }
 */

const mysql = require('mysql');
const thunkify = require('thunkify');
const config = require('./../config');

const pool  = mysql.createPool({
	connectionLimit : config.mysqlConfig.connectionLimit,
	host            : config.mysqlConfig.host,
	user            : config.mysqlConfig.user,
	password        : config.mysqlConfig.password,
	database        : config.mysqlConfig.database,
	multipleStatements: true
});

const executeQuery = function(query, callback) {
	pool.getConnection(function(err, connection) {
		// Use the connection
		connection.query(query, function(err, rows, fields) {
			connection.release();
			if(err) {
				err.mysqlQuery = query;
			}

			if(typeof callback === 'function') {
				callback(err, rows);
			}
			// Don't use the connection here, it has been returned to the pool.
		});
	});
};

module.exports = {
	executeQuery: thunkify(executeQuery),
	executePlainQuery: executeQuery
};
