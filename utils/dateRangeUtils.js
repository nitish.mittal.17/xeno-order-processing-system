const moment = require('moment');

/**
 * Splits the date range into ranges for yearly, monthly and daily tables
 *
 * Example: if start time is 01-01-2012 and end time is current date, then result would be
 *
 * [ { mode: 'yearly',
    startTime: moment("2012-01-01T00:00:00.000"),
    endTime: moment("2018-01-01T00:00:00.000") },
 { mode: 'monthly',
   startTime: moment("2019-01-01T00:00:00.000"),
   endTime: moment("2019-02-01T00:00:00.000") },
 { mode: 'daily',
   startTime: moment("2019-03-01T00:00:00.000"),
   endTime: moment("2019-03-29T00:00:00.000") } ]
 *
 * @param startTimer
 * @param endTimer
 * @returns {Array}
 */
function splitDateRange(startTimer, endTimer) {
	let resolvedRanges = [];

	function splitDate(startTimer, endTimer) {
		let nextDateToCompare = '';
		let lastDateToCompare = '';
		if(endTimer.diff(startTimer, 'year') > 0) {
			if(endTimer.diff(startTimer, 'year') === 1 && startTimer.month() === 0 && startTimer.date() === 1 && endTimer.date() === 1 && endTimer.month() === 0) {
				resolvedRanges.push({mode: "yearly", startTime: startTimer, endTime: endTimer})
			} else {
				nextDateToCompare = getNextYear(startTimer);
				splitDate(startTimer, nextDateToCompare);
				splitDate(nextDateToCompare, endTimer);
			}
		} else if(endTimer.diff(startTimer, 'month') > 0) {
			if(endTimer.diff(startTimer, 'month') === 1 && startTimer.date() === 1 && endTimer.date() === 1) {
				resolvedRanges.push({mode: "monthly", startTime: startTimer, endTime: endTimer})
			} else {
				nextDateToCompare = getNextMonth(startTimer);
				splitDate(startTimer, nextDateToCompare);
				splitDate(nextDateToCompare, endTimer);
			}
		} else if(endTimer.diff(startTimer, 'day') > 0) {
			if (endTimer.diff(startTimer, 'day') === 1 && startTimer.hour() === 0 && startTimer.minute() === 0 &&
				startTimer.second() === 0 && endTimer.hour() === 0 && endTimer.minute() === 0 && endTimer.second() === 0) {
				resolvedRanges.push({mode: "daily", startTime: startTimer, endTime: endTimer})
			} else {
				nextDateToCompare = getNextDay(startTimer);
				splitDate(startTimer, nextDateToCompare);
				splitDate(nextDateToCompare, endTimer);
			}

		}
	}

	splitDate(startTimer, endTimer);

	let finalRanges = [];
	let prevMode = "";
	let currentRange = {};

	for(let x in resolvedRanges) {
		let result = resolvedRanges[x];

		if(result.mode === prevMode) {
			currentRange.endTime = result.startTime;

		} else {
			if(prevMode !== "") {
				finalRanges.push(currentRange);
			}
			currentRange = {};
			currentRange = result;
			prevMode = result.mode;
		}
	}
	finalRanges.push(currentRange);

	return finalRanges;

}
function getNextYear(date) {
	let newYear = date.year() + 1;
	let input = moment(date);
	return input.clone().startOf('year').year(newYear);
}

function getNextMonth(date) {
	let month = date.month() + 1;
	let input = moment(date);
	let output = input.clone().startOf('month').month(month);
	return output > input ? output : output.add(1, 'years');
}

function getNextDay(date) {
	let nextDate = date.date() + 1;
	let input = moment(date);
	let output = input.clone().startOf('day').date(nextDate);
	output = output > input ? output : output.add(1, 'month');
	return output > input ? output : output.add(1, 'years');
}


module.exports = {
	splitDateRange: splitDateRange
};
