function run (genFn) {
	let gen = genFn();
	next();

	function next (er, value) {
		if (er) return gen.throw(er);
		let continuable = gen.next(value);

		if (continuable.done) return;
		let cbFn = continuable.value;
		cbFn(next)
	}
}

function getRandomNumberBetween(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomTimeBetween(start, end) {
	// get the difference between the 2 dates, multiply it by 0-1,
	// and add it to the start date to get a new date
	let diff =  end.getTime() - start.getTime();
	let new_diff = diff * Math.random();
	let date = new Date(start.getTime() + new_diff);
	return date;
}

module.exports = {
	run: run,
	getRandomNumberBetween: getRandomNumberBetween,
	getRandomTimeBetween: getRandomTimeBetween
};