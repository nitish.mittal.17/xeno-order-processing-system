const mysqlUtils = require('./../utils/mysqlUtils');
const dateRangeUtils = require('./../utils/dateRangeUtils');
const moment = require('moment');
const thunkify = require('thunkify');

const tables = {
	yearly: 'order_data_yearly',
	monthly: 'order_data_monthly',
	daily: 'order_data_daily'
};

const constructSelectQuery = (tableName, params, conditions, startTime, endTime) => {
	let queryString = `select ${params.join(',')} from ${tableName} where order_time >= '${startTime}' and order_time <= '${endTime}' and `;
	let arr = [];
	for (let i in conditions) {
		arr.push(`${i} = ${conditions[i]}`);
	}
	queryString += arr.join(' and ');
	return queryString;
};

module.exports = {
	getAggregatedCustomerData: thunkify(function(contactNumber, options, requiredResult, callback) {
		let startDate = moment(options.startTime);
		let endDate = moment(options.endTime);
		let dateRanges = dateRangeUtils.splitDateRange(startDate,endDate);
		delete options.startTime;
		delete options.endTime;

		options.contact_number = contactNumber;

		let result = {};
		let promiseArray = [];
		dateRanges.forEach(dateRange => {
			let queryString = constructSelectQuery(tables[dateRange.mode], requiredResult, options, dateRange.startTime.format('YYYY-MM-DD'), dateRange.endTime.format('YYYY-MM-DD'));
			promiseArray.push(new Promise(function(resolve, reject) {
				mysqlUtils.executePlainQuery(queryString, (err, res) => {
					if (!err) {
						requiredResult.forEach(resultKey => {
							if (!result[resultKey]) {
								result[resultKey] = 0;
							}
							result[resultKey] += res[0][resultKey];
							resolve();
						})
					} else {
						reject();
					}
				})
			}));
		});

		Promise.all(promiseArray).then(() => {
			callback(null, result);
		})
	}),

	getDataRowsForCustomer: thunkify(function(contactNumber, options, requiredResult, callback) {
		let startDate = moment(options.startTime);
		let endDate = moment(options.endTime);
		let dateRanges = dateRangeUtils.splitDateRange(startDate,endDate);
		delete options.startTime;
		delete options.endTime;

		options.contact_number = contactNumber;

		let result = [];
		let promiseArray = [];
		dateRanges.forEach(dateRange => {
			let queryString = constructSelectQuery(tables[dateRange.mode], requiredResult, options, dateRange.startTime.format('YYYY-MM-DD'), dateRange.endTime.format('YYYY-MM-DD'));
			promiseArray.push(new Promise(function(resolve, reject) {
				mysqlUtils.executePlainQuery(queryString, (err, res) => {
					if (!err) {
						result = result.concat(res);
						resolve();
					} else {
						console.log(err);
						reject();
					}
				})
			}));
		});

		Promise.all(promiseArray).then(() => {
			callback(null, result);
		})
	})
};