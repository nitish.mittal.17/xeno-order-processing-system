const fs = require('fs');
const stringify = require('csv-stringify');
const moment = require('moment');

const appUtils = require('./utils/appUtils');

let wstream = fs.createWriteStream('sample-data.csv');

let totalRecords = 1000;
let maxProductsPerOrder = 5;
let i = 0, orderId = 1;
let numberOfStores = 15;
let numOfCustomers = 30;

let contacts = [];
for (let x = 0; x < numOfCustomers; x++) {
	contacts.push(appUtils.getRandomNumberBetween(8000000000, 9999999999));
}

let records = [];

while (i < totalRecords) {
	let contactNumber = contacts[appUtils.getRandomNumberBetween(0,numOfCustomers - 1)];
	let productsForThisOrder = appUtils.getRandomNumberBetween(1, maxProductsPerOrder);
	let orderTime = moment(appUtils.getRandomTimeBetween(new Date("01-01-2010"), new Date()));
	let storeLocation = appUtils.getRandomNumberBetween(1, numberOfStores);

	for (let x = 0; x < productsForThisOrder; x++) {
		records.push({
			'order_id': orderId,
			'contact_number': contactNumber,
			'order_amount': appUtils.getRandomNumberBetween(500, 800),
			'order_time': orderTime.format('YYYY-MM-DD HH:mm:ss'),
			'store_location': storeLocation,
			'product_id': 1000 + x
		});

		i++;
	}

	++orderId;
};

stringify(records, {
	header: true,
	columns: [
		{ key: 'order_id', header: 'order_id'},
		{ key: 'contact_number',header: 'contact_number'},
		{ key: 'order_amount',header: 'order_amount'},
		{ key: 'order_time',header: 'order_time'},
		{ key: 'store_location',header: 'store_location'},
		{ key: 'product_id',header: 'product_id'}
	]
}).pipe(wstream);

