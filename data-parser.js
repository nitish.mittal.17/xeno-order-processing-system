const fs = require('fs');
const parse = require('csv-parse');
const moment = require('moment');
const appUtils = require('./utils/appUtils');
const mysqlUtils = require('./utils/mysqlUtils');

const startTime = new Date();

const generateInsertUpdateQuery = (tableName, params) => {
	let queryString = `insert into ${tableName} (`;
	let fieldValues = params.map(param => param.key);
	queryString += fieldValues.join(',');
	queryString += ') values(';
	let rowValues = [];
	params.forEach(param => {

		if (typeof param.value === 'boolean') {
			param.value = param.value ? '1' : '0';
		}

		if (param.value || param.value === 0) {
			rowValues.push("'" + param.value + "'");
		} else {
			rowValues.push('null');
		}
	});
	queryString += rowValues.join(',');
	queryString += ')';

	queryString += ' ON DUPLICATE KEY UPDATE ';

	let keyValuePairs = [];
	params.forEach(param => {
		if (param.updateOnRepeat) {
			keyValuePairs.push(param.key + '=' + param.key + " + " + param.value);
		}
	});
	queryString += keyValuePairs.join(',');

	return queryString;
};

const getTimeSlotFromTime = (timeString) => {
	let momentObj = moment(timeString);
	return momentObj.startOf('hour').hour();
};

let visitedUsers = {};
const isUserVisitCounted = (orderId, contactNumberValue, timeSlotValue, storeLocationValue) => {
	let key = '' + orderId + contactNumberValue + timeSlotValue + storeLocationValue;
	return visitedUsers[key];
};

const setUserVisited = (orderId, contactNumberValue, timeSlotValue, storeLocationValue) => {
	let key = '' + orderId + contactNumberValue + timeSlotValue + storeLocationValue;
	visitedUsers[key] = true;
};

let parser = parse({delimiter: ',', columns: true});
let promiseArray = [];
let batchSize = 25, batchQueries = [];

fs.createReadStream('sample-data-1000.csv').pipe(parser).on('data', (data) => {
	//Create entry in yearly table

	let timeSlot = getTimeSlotFromTime(data.order_time);

	let yearlyOrderTime = moment(data.order_time).startOf('year').format('YYYY-MM-DD');
	let monthlyOrderTime = moment(data.order_time).startOf('month').format('YYYY-MM-DD');
	let dailyOrderTime = moment(data.order_time).startOf('day').format('YYYY-MM-DD');

	let combinations = [
		{_customer: 0, _time_slot: 0, _store_location: 0},
		{_customer: 0, _time_slot: 0, _store_location: 1},
		{_customer: 0, _time_slot: 1, _store_location: 0},
		{_customer: 0, _time_slot: 1, _store_location: 1},
		{_customer: 1, _time_slot: 0, _store_location: 0},
		{_customer: 1, _time_slot: 0, _store_location: 1},
		{_customer: 1, _time_slot: 1, _store_location: 0},
		{_customer: 1, _time_slot: 1, _store_location: 1},
	];

	let tables = [
		{tableName: 'order_data_yearly', orderTime: yearlyOrderTime},
		{tableName: 'order_data_monthly', orderTime: monthlyOrderTime},
		{tableName: 'order_data_daily', orderTime: dailyOrderTime}
	];

	tables.forEach(table => {
		combinations.forEach(combinations => {

			let timeSlotValue = combinations._time_slot ? timeSlot : -1;
			let storeLocationValue = combinations._store_location ? data.store_location : -1;
			let contactNumberValue = combinations._customer ? data.contact_number : -1;
			let userVisitCounted = isUserVisitCounted(data.order_id, contactNumberValue, timeSlotValue, storeLocationValue);

			let queryString = generateInsertUpdateQuery(table.tableName, [
				{key: 'order_time', value: table.orderTime},
				{key: '_customer', value: combinations._customer},
				{key: '_time_slot', value: combinations._time_slot},
				{key: '_store_location', value: combinations._store_location},
				{key: 'contact_number', value: contactNumberValue},
				{key: 'time_slot', value: timeSlotValue},
				{key: 'store_location', value: storeLocationValue},
				{key: 'order_count', value: 1, updateOnRepeat: !userVisitCounted},
				{key: 'amount_spent', value: data.order_amount, updateOnRepeat: true},
				{key: 'product_count', value: 1, updateOnRepeat: true}
			]);

			if (!userVisitCounted) {
				setUserVisited(data.order_id, contactNumberValue, timeSlotValue, storeLocationValue);
			}

			/*if (data.contact_number === '8535007941' && table.tableName === 'order_data_yearly' && table.orderTime === '2011-01-01') {
				console.log(data.order_id);
				console.log(queryString);
			}*/

			if (batchQueries.length > batchSize) {
				promiseArray.push(new Promise(function(resolve, reject) {
					mysqlUtils.executePlainQuery(batchQueries.join(';'), (err, res) => {
						if (err) {
							console.log(err);
							reject();
						} else {
							resolve();
						}
					});
					batchQueries = [];
				}));
			} else {
				batchQueries.push(queryString);
			}


		});
	});
}).on('end', () => {
	Promise.all(promiseArray).then(() => {
		console.log('Parsing complete');
		let endTime = new Date();
		console.log('Time taken : ' + (endTime - startTime));
	})
});