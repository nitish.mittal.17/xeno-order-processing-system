const appUtils = require('./utils/appUtils');
const orderService = require('./services/orderService');
const moment = require('moment');

appUtils.run(function* () {

	let customerContactNumber = '8535007941';
	let beginningDate = "2010-01-01";
	let currentDate = moment().format('YYYY-MM-DD');

	//Get total number of visits of a customer
	let visits = yield orderService.getAggregatedCustomerData(customerContactNumber, {
		_customer: true,
		_time_slot: false,
		_store_location: false,
		startTime: beginningDate,
		endTime: currentDate
	}, ['sum(order_count)']);
	console.log(visits['sum(order_count)']);

	//Get total amount spent by a customer
	let amount = yield orderService.getAggregatedCustomerData(customerContactNumber, {
		_customer: true,
		_time_slot: false,
		_store_location: false,
		startTime: beginningDate,
		endTime: currentDate
	}, ['sum(amount_spent)']);
	console.log(amount['sum(amount_spent)']);

	//Stores visited by the customer
	let locationsVisitedResult = yield orderService.getDataRowsForCustomer(customerContactNumber, {
		_customer: true,
		_time_slot: false,
		_store_location: true,
		startTime: beginningDate,
		endTime: currentDate
	}, ['distinct(store_location)']);
	let locationsVisited = locationsVisitedResult.map(row => row.store_location);
	console.log(locationsVisited);

	//preferred timeslot to visit for a customer on a given store
	let storeLocation = 7;
	let timeSlotsResult = yield orderService.getDataRowsForCustomer(customerContactNumber, {
		_customer: true,
		_time_slot: true,
		_store_location: true,
		store_location: storeLocation,
		startTime: beginningDate,
		endTime: currentDate
	}, ['time_slot', 'order_count']);

	let mostPreferredSlotCount = -1, mostPreferredSlot = -1;
	timeSlotsResult.forEach(timeSlotRow => {
		if (timeSlotRow.order_count > mostPreferredSlotCount) {
			mostPreferredSlotCount = timeSlotRow.order_count;
			mostPreferredSlot = timeSlotRow.time_slot;
		}
	});
	console.log(`Most preferred slot: ${mostPreferredSlot} with ${mostPreferredSlotCount} visits`);
});